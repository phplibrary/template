#SINEVIA TEMPLATE

Class for working with templates

# Installation #

```
#!json

   "repositories": [
        {
            "type": "vcs",

            "url": "https://bitbucket.org/phplibrary/template.git"
        }
    ],
    "require": {
        "sinevia/phplibrary/template": "dev-master"
    },
```

# How to Use? #